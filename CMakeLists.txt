cmake_minimum_required( VERSION 3.2 FATAL_ERROR )

#find_package( AnalysisBase 21.2 REQUIRED )

atlas_subdir( check_jer )

atlas_depends_on_subdirs(
    PRIVATE
    Control/AthToolSupport/AsgTools
    Control/xAODRootAccess
    Event/xAOD/xAODCore
    Event/xAOD/xAODJet
    Reconstruction/Jet/JetCPInterfaces
    Reconstruction/Jet/JetCalibTool
    PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces
)

find_package( ROOT REQUIRED COMPONENTS RIO Hist Tree Net Core )
set(_common
    INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
    LINK_LIBRARIES ${ROOT_LIBRARIES}
    xAODRootAccess
    xAODJet
    AsgTools
    JetCalibToolsLib
    JetCPInterfaces
)

atlas_add_executable( check_jer util/check_jer.cxx ${_common})
