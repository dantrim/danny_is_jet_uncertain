# danny_is_jet_uncertain

# Installation

```bash
mkdir test_area/
cd test_area/
mkdir source/
cd source/
asetup AnalysisBase,21.2.45,here
git clone https://:@gitlab.cern.ch:8443/dantrim/danny_is_jet_uncertain.git
cd ..
mkdir build/
cd build/
cmake ../source
make -j4
source x86*/setup.sh
```

# Running

Once you have run the steps above:

```bash
check_jer <input-file> <n-events>
```

This will produce a file ```jet_sys.root``` which has histograms showing the relative difference between the systematic-shifted jets' pT and the nominal jets' pT.

A sample file is on `/afs/`:
```
/afs/cern.ch/work/d/dantrim/public/jer_file/mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_SUSY2.e6348_e5984_s3126_r10201_r10210_p3401/DAOD_SUSY2.13242894._000777.pool.root.1
```




