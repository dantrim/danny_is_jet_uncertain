// EDM
#include "xAODJet/JetContainer.h"
#include "AsgTools/ToolHandle.h"

// Base
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"
#include "AsgTools/AnaToolHandle.h"

// jet things
#include "JetInterface/IJetSelector.h"
#include "JetCalibTools/IJetCalibrationTool.h"
#include "JetCPInterfaces/ICPJetUncertaintiesTool.h"
//#include "JetResolution/IJERSmearingTool.h"

// sytematic things
#include "PATInterfaces/SystematicSet.h"
#include "PATInterfaces/SystematicsUtil.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "xAODCore/ShallowCopy.h"

// std/stl
#include <iostream>
#include <sstream>
#include <string>
#include <memory>
#include <stdexcept>
using namespace std;

// ROOT
#include "TFile.h"
#include "TH1F.h"

struct SystInfo {
    CP::SystematicSet systset;
    bool affectsKinematics;
};

bool dbg = false;

int main(int argc, char** argv)
{
    const char * algo = argv[0];
    if(argc < 3) {
        cout << "ERROR expect an input file as input" << endl;
        cout << "Usage: " << algo << " <input-file> <n-events>" << endl;
        return 1;
    }
    string input_name = argv[1];
    int n_events = atoi(argv[2]);

    RETURN_CHECK(algo, xAOD::Init());
    xAOD::TEvent event(xAOD::TEvent::kClassAccess);

    std::unique_ptr<TFile> ifile(TFile::Open(input_name.c_str(), "READ"));
    if(!ifile.get() || ifile->IsZombie()) {
        throw std::logic_error("ERROR could not open input file: " + input_name);
    }

    // ROOT File
    TFile* rfile = new TFile("jet_sys.root", "RECREATE");
    vector<TH1F*> sys_histos;

    // get the jet calibration tool
    asg::AnaToolHandle<IJetCalibrationTool> jet_calib_tool;
    asg::AnaToolHandle<ICPJetUncertaintiesTool> jet_uncerts_tool;

    // get jet calibration tool
    stringstream tool_name;
    tool_name << "JetCalibrationTool/";
    tool_name << "JetCalibTool_AntiKt4EMTopo";
    jet_calib_tool.setTypeAndName(tool_name.str());
    string jes_config = "JES_data2017_2016_2015_Recommendation_Aug2018_rel21.config";
    string jes_calib_seq = "JetArea_Residual_EtaJES_GSC_Smear";
    RETURN_CHECK( algo, jet_calib_tool.setProperty("JetCollection", "AntiKt4EMTopo") );
    RETURN_CHECK( algo, jet_calib_tool.setProperty("ConfigFile", jes_config) );
    RETURN_CHECK( algo, jet_calib_tool.setProperty("CalibSequence", jes_calib_seq) );
    RETURN_CHECK( algo, jet_calib_tool.setProperty("IsData", false) );
    RETURN_CHECK( algo, jet_calib_tool.retrieve() );


    // get jet uncertainties tool
    tool_name.str("");
    tool_name << "JetUncertaintiesTool/";
    tool_name << "JetUncertainties_AntiKt4EMTopo";
    jet_uncerts_tool.setTypeAndName(tool_name.str());
    RETURN_CHECK( algo, jet_uncerts_tool.setProperty("JetDefinition", "AntiKt4EMTopo") );
    RETURN_CHECK( algo, jet_uncerts_tool.setProperty("MCType", "MC16") );
    string uncert_config = "rel21/Summer2018/R4_StrongReduction_Scenario1_SimpleJER.config";
    RETURN_CHECK( algo, jet_uncerts_tool.setProperty("ConfigFile", uncert_config) );
    RETURN_CHECK( algo, jet_uncerts_tool.retrieve() );

    const CP::SystematicRegistry& registry = CP::SystematicRegistry::getInstance();
    const CP::SystematicSet& recommendedSystematics = registry.recommendedSystematics();
    vector<SystInfo> sys_info_list;
    
    SystInfo sinfo;
    sinfo.affectsKinematics = false;
    sys_info_list.push_back(sinfo);

    cout << " +++ ----------------------------------------- +++ " << endl;
    for(const auto & sys_set : CP::make_systematics_vector(recommendedSystematics)) {
        for(const auto & sys : sys_set) {
            if(jet_uncerts_tool->isAffectedBySystematic(CP::SystematicVariation(sys.basename(), CP::SystematicVariation::CONTINUOUS))) {
                cout << "Loading jet sys: " << sys.name().c_str() << endl;
                SystInfo sys_info;
                sys_info.systset.insert(sys);
                sys_info_list.push_back(sys_info);

                stringstream htitle;
                htitle << sys.name() << ";(p_{T}^{sys} - p_{T}^{nom}) / p_{T}^{nom};a.u.";
                stringstream hname;
                hname << "h_" << sys.name();
                TH1F* h = new TH1F(hname.str().c_str(), htitle.str().c_str(), 100, 0, -1);
                sys_histos.push_back(h);
            }
        }
    }
    cout << " +++ ----------------------------------------- +++ " << endl;

    // let's roll
    RETURN_CHECK( algo, event.readFrom(ifile.get()) );
    const unsigned long long n_entries = event.getEntries();
    cout << "File has " << n_entries << " total events";
    if(n_events < 0) n_events = n_entries;
    cout << ", will read " << n_events << " events" << endl;
    for(unsigned long long entry = 0; entry < (unsigned)n_events; ++entry) {

        bool ok = event.getEntry(entry) >= 0;
        if(!ok) throw std::logic_error("ERROR getEntry failed");

        const xAOD::JetContainer* jets = 0;
        RETURN_CHECK( algo, event.retrieve(jets, "AntiKt4EMTopoJets") );
        std::pair<xAOD::JetContainer*, xAOD::ShallowAuxContainer*> shallow = xAOD::shallowCopyContainer(*jets);
        auto jets_copy = shallow.first;

        for(const auto & jet : *jets_copy) {

            jet_uncerts_tool->applySystematicVariation(CP::SystematicSet());
            RETURN_CHECK( algo, jet_calib_tool->applyCalibration(*jet) );

            if( (jet->pt() * 1e-3) < 15 || (fabs(jet->eta()) > 2.8) ) continue;

            jet_uncerts_tool->applyCorrection(*jet);

            float nom_pt = jet->pt() * 1e-3;

            float sys_pt = nom_pt;
            float delta = 0.0;

            // index 0 is "nominal"
            for(int isys = 1; isys < (int)sys_info_list.size(); isys++) {

                // this isn't supposed to be good code...
                xAOD::Jet* sys_jet = new xAOD::Jet(*jet);

                jet_uncerts_tool->applySystematicVariation(sys_info_list.at(isys).systset);
                jet_uncerts_tool->applyCorrection(*sys_jet);

                sys_pt = sys_jet->pt() * 1e-3;
                if(dbg) {
                    cout << " > Systematic " << sys_info_list.at(isys).systset.name() << " : Nominal pT =  " << nom_pt << ", Varied pT = " << sys_pt << "  (Delta = " << (sys_pt - nom_pt) << ")" << endl;
                }

                delta = (sys_pt - nom_pt)/ nom_pt;
                sys_histos.at(isys-1)->Fill(delta);

                // reset tool
                jet_uncerts_tool->applySystematicVariation(CP::SystematicSet());

                delete sys_jet;
                sys_jet = nullptr;
            }
        }
    } // entry

    rfile->cd();
    for(auto & h : sys_histos) {
        h->Write();
    }
    
    
    return 0;
}
